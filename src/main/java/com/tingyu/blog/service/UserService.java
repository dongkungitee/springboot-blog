package com.tingyu.blog.service;

import com.tingyu.blog.po.User;

public interface UserService {

    User checkUser(String username,String password);
}
