package com.tingyu.blog.service;


import com.tingyu.blog.po.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> listCommentByBlogId(int blogId);  //根据博客id获取评论列表

    Comment saveComment(Comment comment);   //保存评论
}
