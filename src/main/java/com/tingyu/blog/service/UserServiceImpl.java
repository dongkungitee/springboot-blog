package com.tingyu.blog.service;

import com.tingyu.blog.dao.UserRepository;
import com.tingyu.blog.po.User;
import com.tingyu.blog.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User checkUser(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username, MD5Utils.md5Encode(password));
        return user;
    }
}
