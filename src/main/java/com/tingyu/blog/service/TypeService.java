package com.tingyu.blog.service;

import com.tingyu.blog.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TypeService {

    Type saveType(Type type);   //保存添加的分类

    Type getOneType(int id);    //通过id获取一个分类

    Type getOneTypeByName(String name);     //通过名称获取一个分类对象

    Page<Type> listType(Pageable pageable);     //分页查询

    List<Type> listTypeTop(int size);    //获取列表前几条数据

    List<Type> listType();  //查询全部分类

    Type editType(int id,Type type);    //编辑分类

    void deleteType(int id);    //删除分类
}
