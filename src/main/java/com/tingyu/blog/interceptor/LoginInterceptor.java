package com.tingyu.blog.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        System.out.println(request.getRequestURI().toString());

        Object object = request.getSession().getAttribute("user");
        if (object == null) {
            System.out.println("用户没有登录");
            response.sendRedirect("/admin");
            return false;   //false 请求不提交了
        }

        //继续提交请求
        return true;

    }
}
