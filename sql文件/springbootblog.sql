/*
 Navicat Premium Data Transfer

 Source Server         : 本地连接1
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : springbootblog

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 05/08/2021 19:26:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (32);
INSERT INTO `hibernate_sequence` VALUES (32);
INSERT INTO `hibernate_sequence` VALUES (32);
INSERT INTO `hibernate_sequence` VALUES (32);
INSERT INTO `hibernate_sequence` VALUES (32);

-- ----------------------------
-- Table structure for tb_blog
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog`;
CREATE TABLE `tb_blog`  (
  `id` int(11) NOT NULL,
  `appreciation` bit(1) NOT NULL,
  `comment_abled` bit(1) NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `create_time` datetime(6) DEFAULT NULL,
  `first_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `flag` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `published` bit(1) NOT NULL,
  `recommend` bit(1) NOT NULL,
  `share_statement` bit(1) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `views` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK789c9u6dqf5vu8tgb0c8w9hem`(`type_id`) USING BTREE,
  INDEX `FKha24n1lxdp27joiq3aqsvuem6`(`user_id`) USING BTREE,
  CONSTRAINT `FK789c9u6dqf5vu8tgb0c8w9hem` FOREIGN KEY (`type_id`) REFERENCES `tb_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKha24n1lxdp27joiq3aqsvuem6` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog
-- ----------------------------
INSERT INTO `tb_blog` VALUES (12, b'1', b'1', '## C#弹出ContextMenuStrip菜单时禁用某些选项\r\n\r\n\r\n```csharp\r\n//菜单弹出前根据条件禁用某些菜单\r\nprivate void CmsRoomRightBtn_Opening(object sender, CancelEventArgs e)\r\n{\r\n    UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;//获取与ContextMenuStrip关联的控件，如 button  （此处是我本人的自定义控件）\r\n    //禁用某些菜单\r\n    锁定ToolStripMenuItem.Enabled = false;\r\n}\r\n```\r\n', '2020-03-11 14:50:11.821000', '', '原创', b'1', b'1', b'1', 'C#弹出ContextMenuStrip菜单时禁用某些选项', '2020-03-14 03:10:54.115000', 6, 3, 1, 'C#弹出ContextMenuStrip菜单时禁用某些选项');
INSERT INTO `tb_blog` VALUES (13, b'0', b'0', '# elementary os桌面不能放图标\r\n#### 需自己安装软件\r\n```shell\r\n安装 desktop folder 以支持桌面管理\r\n应用商店搜索 desktop folder --> 点击安装--> 应用列表里找到点击启动即可。\r\n应用列表图标可直接拖放到桌面\r\n```', '2020-03-11 14:50:11.821000', '', '转载', b'1', b'1', b'0', 'elementary os桌面不能放图标', '2020-03-21 04:36:17.988000', 1, 6, 1, 'elementary os桌面不能放图标，需自己安装desktop folder软件。');
INSERT INTO `tb_blog` VALUES (14, b'0', b'1', '# ubuntu等使用Kali Linux的kali-undercover仿windows 10主题\r\n转载至： [https://www.cnblogs.com/xuyaowen/p/ubuntu-to-use-kali-undercover.html](https://www.cnblogs.com/xuyaowen/p/ubuntu-to-use-kali-undercover.html)\r\n\r\n### 1.加入Kali源\r\n编辑 /etc/apt/sources.list 文件, 在文件最前面添加以下条目：\r\n\r\n\r\n```bash\r\ndeb https://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib\r\ndeb-src https://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib\r\n```\r\n\r\n更改完 sources.list 文件后请运行 `sudo apt-get update` 更新索引以生效。\r\n\r\n### 2.获取 公钥\r\n\r\n```bash\r\napt-key adv --keyserver keyserver.ubuntu.com --recv ED444FF07D8D0BF6\r\n```\r\n\r\n```bash\r\nsudo apt update\r\n```\r\n\r\n如果不进行获取公钥，会出现公钥不能用的错误；\r\nThe following signatures couldn\'t be verified because the public key is not available: NO_PUBKEY ED444FF07D8D0BF6\r\n\r\n### 3.搜索kali-undercover\r\n\r\n```bash\r\napt search kali-undercover\r\n```\r\n\r\n### 4.安装kali-undercover\r\n\r\n```bash\r\napt install kali-undercover\r\n```\r\n\r\n### 5.使用\r\n安装过后，在终端中输入：`kali-undercover` 启动\r\n最后祝您使用愉快；\r\n如果你安装主题后，记得把原来的源换回来；\r\n###### 注意：桌面环境需使用xfce', '2020-03-11 14:50:11.821000', '', '转载', b'1', b'1', b'0', 'ubuntu等使用Kali Linux的kali-undercover仿windows 10主题', '2020-03-14 03:10:43.091000', 2, 6, 1, 'ubuntu等使用Kali Linux的kali-undercover仿windows 10主题');

-- ----------------------------
-- Table structure for tb_blog_tags
-- ----------------------------
DROP TABLE IF EXISTS `tb_blog_tags`;
CREATE TABLE `tb_blog_tags`  (
  `blogs_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL,
  INDEX `FKl133cqmpsfr98uq90a9cuuma7`(`tags_id`) USING BTREE,
  INDEX `FKqkynpmid0t1102koh5icrfeo2`(`blogs_id`) USING BTREE,
  CONSTRAINT `FKl133cqmpsfr98uq90a9cuuma7` FOREIGN KEY (`tags_id`) REFERENCES `tb_tag` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKqkynpmid0t1102koh5icrfeo2` FOREIGN KEY (`blogs_id`) REFERENCES `tb_blog` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_blog_tags
-- ----------------------------
INSERT INTO `tb_blog_tags` VALUES (14, 11);
INSERT INTO `tb_blog_tags` VALUES (13, 1);

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment`  (
  `id` int(11) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `parent_comment_id` int(11) DEFAULT NULL,
  `admin_comment` bit(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK6tc9ipjhoo0ugxqehmv2m6vse`(`blog_id`) USING BTREE,
  INDEX `FK6lk1d0spfxribt4tdt81hlpl6`(`parent_comment_id`) USING BTREE,
  CONSTRAINT `FK6lk1d0spfxribt4tdt81hlpl6` FOREIGN KEY (`parent_comment_id`) REFERENCES `tb_comment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK6tc9ipjhoo0ugxqehmv2m6vse` FOREIGN KEY (`blog_id`) REFERENCES `tb_blog` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
INSERT INTO `tb_comment` VALUES (22, '/img/touxiang.jpg', '小白的测试评论', '2020-03-18 16:19:53.439000', 'ygf@163.com', '小白', 13, NULL, b'0');
INSERT INTO `tb_comment` VALUES (23, '/img/touxiang.jpg', '小红回复小白的测试评论', '2020-03-18 16:20:09.821000', 'ygf@163.com', '小红', 13, 22, b'0');
INSERT INTO `tb_comment` VALUES (24, '/img/touxiang.jpg', '小白回复小红的测试评论', '2020-03-18 16:20:40.432000', 'ygf@163.com', '小白', 13, 23, b'0');
INSERT INTO `tb_comment` VALUES (25, '/img/qibao.jpg', '管理员的评论信息', '2020-03-19 14:12:40.454000', '1186472484@qq.com', '*听雨*', 13, NULL, b'1');
INSERT INTO `tb_comment` VALUES (26, '/img/qibao.jpg', '管理员的回复信息', '2020-03-19 14:13:35.604000', '1186472484@qq.com', '*听雨*', 13, 22, b'1');
INSERT INTO `tb_comment` VALUES (27, '/img/touxiang.jpg', '测试', '2020-04-29 13:55:11.769000', 'ygf963@163.com', '测试', 12, NULL, b'0');
INSERT INTO `tb_comment` VALUES (28, '/img/touxiang.jpg', '测试', '2020-04-29 13:55:51.926000', 'ygf963@163.com', '测试', 12, NULL, b'0');
INSERT INTO `tb_comment` VALUES (30, '/img/touxiang.jpg', '测试', '2020-04-29 14:02:45.801000', 'ygf963@163.com', '测试', 12, NULL, b'0');
INSERT INTO `tb_comment` VALUES (31, '/img/touxiang.jpg', '测试', '2020-04-29 14:02:45.822000', 'ygf963@163.com', '测试', 12, NULL, b'0');

-- ----------------------------
-- Table structure for tb_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_tag`;
CREATE TABLE `tb_tag`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_tag
-- ----------------------------
INSERT INTO `tb_tag` VALUES (1, 'elementary os');
INSERT INTO `tb_tag` VALUES (2, 'winform');
INSERT INTO `tb_tag` VALUES (11, 'Ubuntu');

-- ----------------------------
-- Table structure for tb_type
-- ----------------------------
DROP TABLE IF EXISTS `tb_type`;
CREATE TABLE `tb_type`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_type
-- ----------------------------
INSERT INTO `tb_type` VALUES (1, 'SpringBoot');
INSERT INTO `tb_type` VALUES (2, 'Java');
INSERT INTO `tb_type` VALUES (3, 'C#');
INSERT INTO `tb_type` VALUES (4, 'Python');
INSERT INTO `tb_type` VALUES (5, 'Django');
INSERT INTO `tb_type` VALUES (6, 'Linux');
INSERT INTO `tb_type` VALUES (10, 'Windows');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(11) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `creat_time` datetime(6) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) NOT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, '/img/qibao.jpg', '2020-03-05 22:37:28.000000', '1186472484@qq.com', '*听雨*', 'e10adc3949ba59abbe56e057f20f883e', 0, '2020-03-05 22:38:30.000000', '18788555384');

SET FOREIGN_KEY_CHECKS = 1;
